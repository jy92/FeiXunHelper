﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FeiXunHelper
{
    class ConfigStorate
    {
        public static void SaveUser(string mobile, string cookie)
        {
            TDLib.Utils.AppSettingsUtil.SetValue("user_"+mobile, cookie);
        }

        public static string ReadUserCookie(string mobile)
        {
            return TDLib.Utils.AppSettingsUtil.GetValue("user_" + mobile, null);
        }

        public static List<string> ReadAllUsers()
        {
            List<string> list = new List<string>();
            var keys = TDLib.Utils.AppSettingsUtil.GetKeys();
            foreach (var key in keys)
            {
                if (key.StartsWith("user_"))
                {
                    string mobile = key.Remove(0, "user_".Length);
                    list.Add(mobile);
                }
            }
            return list;
        }
    }
}
