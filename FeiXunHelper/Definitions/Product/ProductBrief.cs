﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FeiXunHelper.Definitions.Product
{
    public class ProductBrief
    {
        // 商品id
        public string goods_id;
        // 商品名称
        public string name;
        // 商品描述
        public string brief;
        // 商品对象
        public ProductBriefObj product;
        // 商品图片
        public string image;
    }
}
