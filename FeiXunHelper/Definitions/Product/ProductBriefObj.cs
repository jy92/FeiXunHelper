﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FeiXunHelper.Definitions.Product
{
    public class ProductBriefObj
    {
        // 产品id
        public string product_id;
        // 商品id
        public string goods_id;
        // 售价￥
        public double price;
        // 原价￥
        public double mktprice;
        // item url地址，如：https://mall.phicomm.com/item-11.html
        public string item_url;
    }
}
