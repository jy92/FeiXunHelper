﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FeiXunHelper.Definitions.Product
{
    public class ProductDetailObj
    {
        // 产品id
        public string product_id;
        // 售价￥
        public double saleprice;
        // 原价￥
        public double mktprice;
    }
}
