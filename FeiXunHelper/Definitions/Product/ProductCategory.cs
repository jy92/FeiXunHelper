﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FeiXunHelper.Definitions.Product
{
    public class ProductCategory
    {
        public string Id { get; private set; }
        public string Name { get; private set; }

        private ProductCategory(string id, string name)
        {
            this.Id = id;
            this.Name = name;
        }

        public override string ToString()
        {
            return this.Name;
        }

        public static readonly ProductCategory JiaTingKuanDai = new ProductCategory("https://mall.phicomm.com/m/index-001.html", "家庭宽带");
        public static readonly ProductCategory ZhiNengJiaJu = new ProductCategory("https://mall.phicomm.com/m/index-002.html", "智能家居");
        public static readonly ProductCategory YunDongJianKang = new ProductCategory("https://mall.phicomm.com/m/index-03.html", "运动健康");
        public static readonly ProductCategory QuKuaiLian = new ProductCategory("https://mall.phicomm.com/m/index-444.html", "区块链");
        public static readonly List<ProductCategory> AllCategory = new List<ProductCategory>();

        static ProductCategory()
        {
            AllCategory = new List<ProductCategory>() { 
                JiaTingKuanDai,
                ZhiNengJiaJu,
                YunDongJianKang,
                //QuKuaiLian
            };
        }
    }
}
