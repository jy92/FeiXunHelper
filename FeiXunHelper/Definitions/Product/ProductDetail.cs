﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Text;
using Newtonsoft.Json;

namespace FeiXunHelper.Definitions.Product
{
    public class ProductDetail
    {
        // 商品id
        public string goods_id;
        // 商品名称
        public string name;
        // 商品描述
        public string brief;
        // 已售件数
        public int buy_count;
        // 评论数
        public int comment_count;
        // 商品是否上架
        public bool marketable;
        // 剩余件数
        public int stock;
        public string nostore_sell;
        public bool nature_source;
        // 商品对象
        public ProductDetailObj product;
        // 图片
        public List<ProductDetailImage> images;

        /// <summary>
        /// 是否已下架
        /// </summary>
        /// <returns></returns>
        public bool IsOff()
        {
            return this.marketable == false;
        }

        /// <summary>
        /// 是否已售完
        /// </summary>
        /// <returns></returns>
        public bool IsSellOut()
        {
            return this.nostore_sell == "0" && this.stock == 0;
        }

        /// <summary>
        /// 是否可加入购物车
        /// </summary>
        /// <returns></returns>
        public bool IsCanAddCart()
        {
            return this.marketable && (this.nostore_sell == "1" || (this.stock > 0 && !this.nature_source));
        }

        /// <summary>
        /// 是否可立即购买
        /// </summary>
        /// <returns></returns>
        public bool IsCanBuyGoods()
        {
            return this.marketable && (this.nostore_sell == "1" || this.stock > 0);
        }

        public override string ToString()
        {
            return this.name;
        }
    }
}
