﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FeiXunHelper.Definitions.Product
{
    public class ProductDetailImage
    {
        public string image_id;
        public string url;
        public string l_url;
        public string m_url;
        public string s_url;
        public string xs_url;
    }
}
