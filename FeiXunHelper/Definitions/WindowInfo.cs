﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FeiXunHelper.Definitions
{
    class WindowInfo
    {
        public IntPtr Hwnd;
        public string Name;

        public override string ToString()
        {
            return this.Name;
        }
    }
}
