﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FeiXunHelper.Definitions.Address
{
    public class AddressBrief
    {
        public string Name;
        public string Mobile;
        public string Province;
        public string City;
        public string Country;
        public string Street;
    }
}
