﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FeiXunHelper.Definitions.Address
{
    public class AddressDetail
    {
        public string name;
        public string addr_id;
        public string city;
        // mainland:北京市/市辖区/房山区:12
        public string area;
        public string addr;
        public string mobile;
        public bool is_default;
        public string updatetime;

        private List<string> _parts;
        private void SplitArea()
        {
            if (_parts != null)
                return;
            _parts = this.area.Split(":/".ToCharArray(), StringSplitOptions.RemoveEmptyEntries).ToList();
            _parts.RemoveAt(0);
            _parts.RemoveAt(_parts.Count - 1);
        }
        public string GetProvince()
        {
            SplitArea();
            return _parts[0];
        }
        public string GetCity()
        {
            SplitArea();
            return _parts[1];
        }
        public string GetCountry()
        {
            SplitArea();
            return _parts[2];
        }
        public override string ToString()
        {
            return string.Format("{0},{1},{2},{3},{4},{5}",
                this.name, this.mobile, GetProvince(), GetCity(), GetCountry(), this.addr);
        }
    }
}
