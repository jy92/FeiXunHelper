﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace FeiXunHelper.UIs.Common
{
    public partial class ColorBlock : UserControl
    {
        public ColorBlock()
        {
            InitializeComponent();
        }

        private void ColorBlock_MouseClick(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                ColorDialog dialog = new ColorDialog();
                if (dialog.ShowDialog() == DialogResult.OK)
                {
                    this.BackColor = dialog.Color;
                }
            }
        }
    }
}
