﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TDLib.UIs.Forms;
using FeiXunHelper.Definitions;
using FeiXunHelper.Services;
using TDLib.Utils;
using FeiXunHelper.Definitions.Address;
using FeiXunHelper.UIs.Common;

namespace FeiXunHelper.UIs.Query
{
    public partial class FrmSelectAddress : FrmIconBase
    {
        private FeiXunService _service;
        public AddressDetail SelectedAddress { get; private set; }
        public FrmSelectAddress(FeiXunService service)
        {
            InitializeComponent();
            _service = service;
        }

        private void listViewAddressList_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            if (this.listViewAddressList.SelectedItems.Count > 0)
            {
                var address = this.listViewAddressList.SelectedItems[0].Tag as AddressDetail;
                this.SelectedAddress = address;
                this.DialogResult = System.Windows.Forms.DialogResult.OK;
            }
        }

        private void FrmQueryAddress_Load(object sender, EventArgs e)
        {
            RunTaskThread(() =>
            {
                this.listViewAddressList.Items.Clear();
                try
                {
                    var result = _service.QueryAddressList();
                    for (int i = 0; i < result.Count; i++)
                    {
                        var address = result[i];
                        var item = this.listViewAddressList.Items.Add(new ListViewItem(new string[]{
                        (i+1).ToString(),
                        address.addr_id,
                        address.name,
                        address.mobile,
                        address.GetProvince(),
                        address.GetCity(),
                        address.GetCountry(),
                        address.addr
                        }));
                        item.Tag = address;
                    }
                }
                catch (Exception ex)
                {
                    MsgBox.Warning("加载收货地址失败," + ex.Message);
                }
            });
        }
    }
}
