﻿namespace FeiXunHelper.UIs.Query
{
    partial class FrmSelectAddress
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.listViewAddressList = new TDLib.UIs.Controls.ListViewExNoBlink();
            this.columnHeader10 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader12 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader18 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader13 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader14 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader15 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader16 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader17 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.SuspendLayout();
            // 
            // listViewAddressList
            // 
            this.listViewAddressList.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader10,
            this.columnHeader12,
            this.columnHeader18,
            this.columnHeader13,
            this.columnHeader14,
            this.columnHeader15,
            this.columnHeader16,
            this.columnHeader17});
            this.listViewAddressList.Dock = System.Windows.Forms.DockStyle.Fill;
            this.listViewAddressList.FullRowSelect = true;
            this.listViewAddressList.GridLines = true;
            this.listViewAddressList.Location = new System.Drawing.Point(0, 0);
            this.listViewAddressList.Name = "listViewAddressList";
            this.listViewAddressList.Size = new System.Drawing.Size(756, 346);
            this.listViewAddressList.TabIndex = 3;
            this.listViewAddressList.UseCompatibleStateImageBehavior = false;
            this.listViewAddressList.View = System.Windows.Forms.View.Details;
            this.listViewAddressList.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.listViewAddressList_MouseDoubleClick);
            // 
            // columnHeader10
            // 
            this.columnHeader10.Text = "#";
            // 
            // columnHeader12
            // 
            this.columnHeader12.Text = "编号";
            this.columnHeader12.Width = 100;
            // 
            // columnHeader18
            // 
            this.columnHeader18.Text = "姓名";
            this.columnHeader18.Width = 80;
            // 
            // columnHeader13
            // 
            this.columnHeader13.Text = "手机号";
            this.columnHeader13.Width = 122;
            // 
            // columnHeader14
            // 
            this.columnHeader14.Text = "省";
            this.columnHeader14.Width = 122;
            // 
            // columnHeader15
            // 
            this.columnHeader15.Text = "市";
            this.columnHeader15.Width = 122;
            // 
            // columnHeader16
            // 
            this.columnHeader16.Text = "乡";
            this.columnHeader16.Width = 110;
            // 
            // columnHeader17
            // 
            this.columnHeader17.Text = "街道";
            this.columnHeader17.Width = 110;
            // 
            // FrmQueryAddress
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(756, 346);
            this.Controls.Add(this.listViewAddressList);
            this.Name = "FrmQueryAddress";
            this.Text = "选择收货地址";
            this.Load += new System.EventHandler(this.FrmQueryAddress_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private TDLib.UIs.Controls.ListViewExNoBlink listViewAddressList;
        private System.Windows.Forms.ColumnHeader columnHeader10;
        private System.Windows.Forms.ColumnHeader columnHeader12;
        private System.Windows.Forms.ColumnHeader columnHeader18;
        private System.Windows.Forms.ColumnHeader columnHeader13;
        private System.Windows.Forms.ColumnHeader columnHeader14;
        private System.Windows.Forms.ColumnHeader columnHeader15;
        private System.Windows.Forms.ColumnHeader columnHeader16;
        private System.Windows.Forms.ColumnHeader columnHeader17;

    }
}