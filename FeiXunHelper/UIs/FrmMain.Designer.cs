﻿namespace FeiXunHelper.UIs
{
    partial class FrmMain
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmMain));
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.listViewProductList = new TDLib.UIs.Controls.ListViewExNoBlink();
            this.columnHeader1 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader11 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader2 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader6 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader3 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader7 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader8 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader9 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader4 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.panel2 = new System.Windows.Forms.Panel();
            this.panelSeries = new System.Windows.Forms.Panel();
            this.checkBoxSeriesAll = new System.Windows.Forms.CheckBox();
            this.label1 = new System.Windows.Forms.Label();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.listViewAddressList = new TDLib.UIs.Controls.ListViewExNoBlink();
            this.columnHeader10 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader12 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader18 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader13 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader14 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader15 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader16 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader17 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.toolStrip2 = new System.Windows.Forms.ToolStrip();
            this.toolStripButtonQueryAddress = new System.Windows.Forms.ToolStripButton();
            this.toolStripButtonAddAddress = new System.Windows.Forms.ToolStripButton();
            this.panel1 = new System.Windows.Forms.Panel();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.textBoxOutput = new System.Windows.Forms.TextBox();
            this.tabControl2 = new System.Windows.Forms.TabControl();
            this.tabPage4 = new System.Windows.Forms.TabPage();
            this.checkBoxAutoCreateOrder = new System.Windows.Forms.CheckBox();
            this.checkBoxCustomQueryInternal = new System.Windows.Forms.CheckBox();
            this.numericUpDownQueryInternal = new System.Windows.Forms.NumericUpDown();
            this.buttonSelectAddress = new System.Windows.Forms.Button();
            this.textBoxCurAddress = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.listBoxCurProductList = new System.Windows.Forms.ListBox();
            this.label12 = new System.Windows.Forms.Label();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.colorBlock2 = new FeiXunHelper.UIs.Common.ColorBlock();
            this.label26 = new System.Windows.Forms.Label();
            this.tabPage5 = new System.Windows.Forms.TabPage();
            this.radioButtonCtrlEnter = new System.Windows.Forms.RadioButton();
            this.radioButtonEnter = new System.Windows.Forms.RadioButton();
            this.buttonTestSendQQMsg = new System.Windows.Forms.Button();
            this.buttonGetQQWindow = new System.Windows.Forms.Button();
            this.comboBoxQQWindow = new System.Windows.Forms.ComboBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.tabPage6 = new System.Windows.Forms.TabPage();
            this.buttonTestPlay = new System.Windows.Forms.Button();
            this.comboBoxMusicList = new System.Windows.Forms.ComboBox();
            this.label5 = new System.Windows.Forms.Label();
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.ToolStripMenuItemSeriesSelectAll = new System.Windows.Forms.ToolStripMenuItem();
            this.contextMenuStrip2 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.ToolStripMenuItemStateSelectAll = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.toolStripButtonLoginOrLogout = new System.Windows.Forms.ToolStripButton();
            this.toolStripButtonQuery = new System.Windows.Forms.ToolStripButton();
            this.toolStripButtonMonitor = new System.Windows.Forms.ToolStripButton();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.toolStripStatusLabel1 = new System.Windows.Forms.ToolStripStatusLabel();
            this.visualStyler1 = new SkinSoft.VisualStyler.VisualStyler(this.components);
            this.notifyIcon1 = new System.Windows.Forms.NotifyIcon(this.components);
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panelSeries.SuspendLayout();
            this.tabPage3.SuspendLayout();
            this.toolStrip2.SuspendLayout();
            this.panel1.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.tabControl2.SuspendLayout();
            this.tabPage4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownQueryInternal)).BeginInit();
            this.tabPage2.SuspendLayout();
            this.tabPage5.SuspendLayout();
            this.tabPage6.SuspendLayout();
            this.contextMenuStrip1.SuspendLayout();
            this.contextMenuStrip2.SuspendLayout();
            this.toolStrip1.SuspendLayout();
            this.statusStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.visualStyler1)).BeginInit();
            this.SuspendLayout();
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage3);
            this.tabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl1.Location = new System.Drawing.Point(0, 39);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(1043, 338);
            this.tabControl1.TabIndex = 1;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.listViewProductList);
            this.tabPage1.Controls.Add(this.panel2);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(1035, 312);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "抢购页面";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // listViewProductList
            // 
            this.listViewProductList.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader1,
            this.columnHeader11,
            this.columnHeader2,
            this.columnHeader6,
            this.columnHeader3,
            this.columnHeader7,
            this.columnHeader8,
            this.columnHeader9,
            this.columnHeader4});
            this.listViewProductList.Dock = System.Windows.Forms.DockStyle.Fill;
            this.listViewProductList.FullRowSelect = true;
            this.listViewProductList.GridLines = true;
            this.listViewProductList.Location = new System.Drawing.Point(3, 56);
            this.listViewProductList.Name = "listViewProductList";
            this.listViewProductList.Size = new System.Drawing.Size(1029, 253);
            this.listViewProductList.TabIndex = 1;
            this.listViewProductList.UseCompatibleStateImageBehavior = false;
            this.listViewProductList.View = System.Windows.Forms.View.Details;
            this.listViewProductList.MouseUp += new System.Windows.Forms.MouseEventHandler(this.listViewProductList_MouseUp);
            // 
            // columnHeader1
            // 
            this.columnHeader1.Text = "#";
            // 
            // columnHeader11
            // 
            this.columnHeader11.Text = "种类";
            this.columnHeader11.Width = 100;
            // 
            // columnHeader2
            // 
            this.columnHeader2.Text = "产品编号";
            this.columnHeader2.Width = 122;
            // 
            // columnHeader6
            // 
            this.columnHeader6.Text = "商品编号";
            this.columnHeader6.Width = 122;
            // 
            // columnHeader3
            // 
            this.columnHeader3.Text = "商品名称";
            this.columnHeader3.Width = 366;
            // 
            // columnHeader7
            // 
            this.columnHeader7.Text = "商品价格";
            this.columnHeader7.Width = 110;
            // 
            // columnHeader8
            // 
            this.columnHeader8.Text = "已售出(件)";
            this.columnHeader8.Width = 110;
            // 
            // columnHeader9
            // 
            this.columnHeader9.Text = "剩余(件)";
            this.columnHeader9.Width = 110;
            // 
            // columnHeader4
            // 
            this.columnHeader4.Text = "是否可购";
            this.columnHeader4.Width = 80;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.panelSeries);
            this.panel2.Controls.Add(this.label1);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel2.Location = new System.Drawing.Point(3, 3);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(1029, 53);
            this.panel2.TabIndex = 0;
            // 
            // panelSeries
            // 
            this.panelSeries.Controls.Add(this.checkBoxSeriesAll);
            this.panelSeries.Location = new System.Drawing.Point(88, 10);
            this.panelSeries.Name = "panelSeries";
            this.panelSeries.Size = new System.Drawing.Size(385, 23);
            this.panelSeries.TabIndex = 5;
            // 
            // checkBoxSeriesAll
            // 
            this.checkBoxSeriesAll.AutoSize = true;
            this.checkBoxSeriesAll.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.checkBoxSeriesAll.Location = new System.Drawing.Point(5, 3);
            this.checkBoxSeriesAll.Name = "checkBoxSeriesAll";
            this.checkBoxSeriesAll.Size = new System.Drawing.Size(48, 16);
            this.checkBoxSeriesAll.TabIndex = 4;
            this.checkBoxSeriesAll.Text = "全选";
            this.checkBoxSeriesAll.UseVisualStyleBackColor = true;
            this.checkBoxSeriesAll.CheckedChanged += new System.EventHandler(this.checkBoxSeriesAll_CheckedChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(24, 16);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(65, 12);
            this.label1.TabIndex = 0;
            this.label1.Text = "筛选种类：";
            // 
            // tabPage3
            // 
            this.tabPage3.Controls.Add(this.listViewAddressList);
            this.tabPage3.Controls.Add(this.toolStrip2);
            this.tabPage3.Location = new System.Drawing.Point(4, 22);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Size = new System.Drawing.Size(1035, 312);
            this.tabPage3.TabIndex = 1;
            this.tabPage3.Text = "收货地址";
            this.tabPage3.UseVisualStyleBackColor = true;
            // 
            // listViewAddressList
            // 
            this.listViewAddressList.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader10,
            this.columnHeader12,
            this.columnHeader18,
            this.columnHeader13,
            this.columnHeader14,
            this.columnHeader15,
            this.columnHeader16,
            this.columnHeader17});
            this.listViewAddressList.Dock = System.Windows.Forms.DockStyle.Fill;
            this.listViewAddressList.FullRowSelect = true;
            this.listViewAddressList.GridLines = true;
            this.listViewAddressList.Location = new System.Drawing.Point(0, 25);
            this.listViewAddressList.Name = "listViewAddressList";
            this.listViewAddressList.Size = new System.Drawing.Size(1035, 287);
            this.listViewAddressList.TabIndex = 2;
            this.listViewAddressList.UseCompatibleStateImageBehavior = false;
            this.listViewAddressList.View = System.Windows.Forms.View.Details;
            // 
            // columnHeader10
            // 
            this.columnHeader10.Text = "#";
            // 
            // columnHeader12
            // 
            this.columnHeader12.Text = "编号";
            this.columnHeader12.Width = 100;
            // 
            // columnHeader18
            // 
            this.columnHeader18.Text = "姓名";
            this.columnHeader18.Width = 80;
            // 
            // columnHeader13
            // 
            this.columnHeader13.Text = "手机号";
            this.columnHeader13.Width = 122;
            // 
            // columnHeader14
            // 
            this.columnHeader14.Text = "省";
            this.columnHeader14.Width = 122;
            // 
            // columnHeader15
            // 
            this.columnHeader15.Text = "市";
            this.columnHeader15.Width = 122;
            // 
            // columnHeader16
            // 
            this.columnHeader16.Text = "乡";
            this.columnHeader16.Width = 110;
            // 
            // columnHeader17
            // 
            this.columnHeader17.Text = "街道";
            this.columnHeader17.Width = 110;
            // 
            // toolStrip2
            // 
            this.toolStrip2.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripButtonQueryAddress,
            this.toolStripButtonAddAddress});
            this.toolStrip2.Location = new System.Drawing.Point(0, 0);
            this.toolStrip2.Name = "toolStrip2";
            this.toolStrip2.Size = new System.Drawing.Size(1035, 25);
            this.toolStrip2.TabIndex = 0;
            this.toolStrip2.Text = "toolStrip2";
            // 
            // toolStripButtonQueryAddress
            // 
            this.toolStripButtonQueryAddress.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButtonQueryAddress.Image")));
            this.toolStripButtonQueryAddress.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonQueryAddress.Name = "toolStripButtonQueryAddress";
            this.toolStripButtonQueryAddress.Size = new System.Drawing.Size(52, 22);
            this.toolStripButtonQueryAddress.Text = "查询";
            this.toolStripButtonQueryAddress.Click += new System.EventHandler(this.toolStripButtonQueryAddress_Click);
            // 
            // toolStripButtonAddAddress
            // 
            this.toolStripButtonAddAddress.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButtonAddAddress.Image")));
            this.toolStripButtonAddAddress.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonAddAddress.Name = "toolStripButtonAddAddress";
            this.toolStripButtonAddAddress.Size = new System.Drawing.Size(52, 22);
            this.toolStripButtonAddAddress.Text = "添加";
            this.toolStripButtonAddAddress.Visible = false;
            // 
            // panel1
            // 
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Controls.Add(this.groupBox1);
            this.panel1.Controls.Add(this.tabControl2);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel1.Location = new System.Drawing.Point(0, 377);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1043, 206);
            this.panel1.TabIndex = 3;
            // 
            // groupBox1
            // 
            this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox1.Controls.Add(this.textBoxOutput);
            this.groupBox1.Location = new System.Drawing.Point(597, 5);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(441, 194);
            this.groupBox1.TabIndex = 1;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "输出";
            // 
            // textBoxOutput
            // 
            this.textBoxOutput.Dock = System.Windows.Forms.DockStyle.Fill;
            this.textBoxOutput.Location = new System.Drawing.Point(3, 17);
            this.textBoxOutput.Multiline = true;
            this.textBoxOutput.Name = "textBoxOutput";
            this.textBoxOutput.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.textBoxOutput.Size = new System.Drawing.Size(435, 174);
            this.textBoxOutput.TabIndex = 0;
            // 
            // tabControl2
            // 
            this.tabControl2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)));
            this.tabControl2.Controls.Add(this.tabPage4);
            this.tabControl2.Controls.Add(this.tabPage2);
            this.tabControl2.Controls.Add(this.tabPage5);
            this.tabControl2.Controls.Add(this.tabPage6);
            this.tabControl2.Location = new System.Drawing.Point(6, 8);
            this.tabControl2.Name = "tabControl2";
            this.tabControl2.SelectedIndex = 0;
            this.tabControl2.Size = new System.Drawing.Size(585, 191);
            this.tabControl2.TabIndex = 0;
            // 
            // tabPage4
            // 
            this.tabPage4.Controls.Add(this.checkBoxAutoCreateOrder);
            this.tabPage4.Controls.Add(this.checkBoxCustomQueryInternal);
            this.tabPage4.Controls.Add(this.numericUpDownQueryInternal);
            this.tabPage4.Controls.Add(this.buttonSelectAddress);
            this.tabPage4.Controls.Add(this.textBoxCurAddress);
            this.tabPage4.Controls.Add(this.label2);
            this.tabPage4.Controls.Add(this.listBoxCurProductList);
            this.tabPage4.Controls.Add(this.label12);
            this.tabPage4.Location = new System.Drawing.Point(4, 22);
            this.tabPage4.Name = "tabPage4";
            this.tabPage4.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage4.Size = new System.Drawing.Size(577, 165);
            this.tabPage4.TabIndex = 1;
            this.tabPage4.Text = "抢购设置";
            this.tabPage4.UseVisualStyleBackColor = true;
            // 
            // checkBoxAutoCreateOrder
            // 
            this.checkBoxAutoCreateOrder.AutoSize = true;
            this.checkBoxAutoCreateOrder.Checked = true;
            this.checkBoxAutoCreateOrder.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBoxAutoCreateOrder.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.checkBoxAutoCreateOrder.Location = new System.Drawing.Point(280, 125);
            this.checkBoxAutoCreateOrder.Name = "checkBoxAutoCreateOrder";
            this.checkBoxAutoCreateOrder.Size = new System.Drawing.Size(72, 16);
            this.checkBoxAutoCreateOrder.TabIndex = 10;
            this.checkBoxAutoCreateOrder.Text = "自动下单";
            this.checkBoxAutoCreateOrder.UseVisualStyleBackColor = true;
            // 
            // checkBoxCustomQueryInternal
            // 
            this.checkBoxCustomQueryInternal.AutoSize = true;
            this.checkBoxCustomQueryInternal.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.checkBoxCustomQueryInternal.Location = new System.Drawing.Point(280, 67);
            this.checkBoxCustomQueryInternal.Name = "checkBoxCustomQueryInternal";
            this.checkBoxCustomQueryInternal.Size = new System.Drawing.Size(156, 16);
            this.checkBoxCustomQueryInternal.TabIndex = 9;
            this.checkBoxCustomQueryInternal.Text = "自定义查询间隔(毫秒)：";
            this.checkBoxCustomQueryInternal.UseVisualStyleBackColor = true;
            this.checkBoxCustomQueryInternal.CheckedChanged += new System.EventHandler(this.checkBoxCustomQueryInternal_CheckedChanged);
            // 
            // numericUpDownQueryInternal
            // 
            this.numericUpDownQueryInternal.Enabled = false;
            this.numericUpDownQueryInternal.Location = new System.Drawing.Point(281, 89);
            this.numericUpDownQueryInternal.Maximum = new decimal(new int[] {
            60000,
            0,
            0,
            0});
            this.numericUpDownQueryInternal.Minimum = new decimal(new int[] {
            500,
            0,
            0,
            0});
            this.numericUpDownQueryInternal.Name = "numericUpDownQueryInternal";
            this.numericUpDownQueryInternal.Size = new System.Drawing.Size(145, 21);
            this.numericUpDownQueryInternal.TabIndex = 7;
            this.numericUpDownQueryInternal.Value = new decimal(new int[] {
            3000,
            0,
            0,
            0});
            // 
            // buttonSelectAddress
            // 
            this.buttonSelectAddress.Location = new System.Drawing.Point(525, 29);
            this.buttonSelectAddress.Name = "buttonSelectAddress";
            this.buttonSelectAddress.Size = new System.Drawing.Size(32, 23);
            this.buttonSelectAddress.TabIndex = 4;
            this.buttonSelectAddress.Text = "选";
            this.buttonSelectAddress.UseVisualStyleBackColor = true;
            this.buttonSelectAddress.Click += new System.EventHandler(this.buttonSelectAddress_Click);
            // 
            // textBoxCurAddress
            // 
            this.textBoxCurAddress.BackColor = System.Drawing.Color.White;
            this.textBoxCurAddress.Location = new System.Drawing.Point(279, 30);
            this.textBoxCurAddress.Name = "textBoxCurAddress";
            this.textBoxCurAddress.ReadOnly = true;
            this.textBoxCurAddress.Size = new System.Drawing.Size(240, 21);
            this.textBoxCurAddress.TabIndex = 3;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(279, 13);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(65, 12);
            this.label2.TabIndex = 2;
            this.label2.Text = "收货地址：";
            // 
            // listBoxCurProductList
            // 
            this.listBoxCurProductList.FormattingEnabled = true;
            this.listBoxCurProductList.ItemHeight = 12;
            this.listBoxCurProductList.Location = new System.Drawing.Point(12, 33);
            this.listBoxCurProductList.Name = "listBoxCurProductList";
            this.listBoxCurProductList.SelectionMode = System.Windows.Forms.SelectionMode.MultiSimple;
            this.listBoxCurProductList.Size = new System.Drawing.Size(244, 124);
            this.listBoxCurProductList.TabIndex = 1;
            this.listBoxCurProductList.MouseUp += new System.Windows.Forms.MouseEventHandler(this.listBoxCurProductList_MouseUp);
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(13, 13);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(65, 12);
            this.label12.TabIndex = 0;
            this.label12.Text = "抢购列表：";
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.colorBlock2);
            this.tabPage2.Controls.Add(this.label26);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(577, 165);
            this.tabPage2.TabIndex = 2;
            this.tabPage2.Text = "通用设置";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // colorBlock2
            // 
            this.colorBlock2.BackColor = System.Drawing.Color.Yellow;
            this.colorBlock2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.colorBlock2.Location = new System.Drawing.Point(27, 44);
            this.colorBlock2.Name = "colorBlock2";
            this.colorBlock2.Size = new System.Drawing.Size(131, 23);
            this.colorBlock2.TabIndex = 3;
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Location = new System.Drawing.Point(25, 23);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(65, 12);
            this.label26.TabIndex = 2;
            this.label26.Text = "可购颜色：";
            // 
            // tabPage5
            // 
            this.tabPage5.Controls.Add(this.radioButtonCtrlEnter);
            this.tabPage5.Controls.Add(this.radioButtonEnter);
            this.tabPage5.Controls.Add(this.buttonTestSendQQMsg);
            this.tabPage5.Controls.Add(this.buttonGetQQWindow);
            this.tabPage5.Controls.Add(this.comboBoxQQWindow);
            this.tabPage5.Controls.Add(this.label4);
            this.tabPage5.Controls.Add(this.label3);
            this.tabPage5.Location = new System.Drawing.Point(4, 22);
            this.tabPage5.Name = "tabPage5";
            this.tabPage5.Size = new System.Drawing.Size(577, 165);
            this.tabPage5.TabIndex = 3;
            this.tabPage5.Text = "QQ通知";
            this.tabPage5.UseVisualStyleBackColor = true;
            // 
            // radioButtonCtrlEnter
            // 
            this.radioButtonCtrlEnter.AutoSize = true;
            this.radioButtonCtrlEnter.Checked = true;
            this.radioButtonCtrlEnter.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.radioButtonCtrlEnter.Location = new System.Drawing.Point(120, 115);
            this.radioButtonCtrlEnter.Name = "radioButtonCtrlEnter";
            this.radioButtonCtrlEnter.Size = new System.Drawing.Size(83, 16);
            this.radioButtonCtrlEnter.TabIndex = 3;
            this.radioButtonCtrlEnter.TabStop = true;
            this.radioButtonCtrlEnter.Text = "Ctrl+Enter";
            this.radioButtonCtrlEnter.UseVisualStyleBackColor = true;
            // 
            // radioButtonEnter
            // 
            this.radioButtonEnter.AutoSize = true;
            this.radioButtonEnter.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.radioButtonEnter.Location = new System.Drawing.Point(44, 115);
            this.radioButtonEnter.Name = "radioButtonEnter";
            this.radioButtonEnter.Size = new System.Drawing.Size(53, 16);
            this.radioButtonEnter.TabIndex = 3;
            this.radioButtonEnter.Text = "Enter";
            this.radioButtonEnter.UseVisualStyleBackColor = true;
            // 
            // buttonTestSendQQMsg
            // 
            this.buttonTestSendQQMsg.Location = new System.Drawing.Point(292, 76);
            this.buttonTestSendQQMsg.Name = "buttonTestSendQQMsg";
            this.buttonTestSendQQMsg.Size = new System.Drawing.Size(138, 23);
            this.buttonTestSendQQMsg.TabIndex = 2;
            this.buttonTestSendQQMsg.Text = "测试";
            this.buttonTestSendQQMsg.UseVisualStyleBackColor = true;
            this.buttonTestSendQQMsg.Click += new System.EventHandler(this.buttonTestSendQQMsg_Click);
            // 
            // buttonGetQQWindow
            // 
            this.buttonGetQQWindow.Location = new System.Drawing.Point(292, 47);
            this.buttonGetQQWindow.Name = "buttonGetQQWindow";
            this.buttonGetQQWindow.Size = new System.Drawing.Size(138, 23);
            this.buttonGetQQWindow.TabIndex = 2;
            this.buttonGetQQWindow.Text = "获取聊天窗口";
            this.buttonGetQQWindow.UseVisualStyleBackColor = true;
            this.buttonGetQQWindow.Click += new System.EventHandler(this.buttonGetQQWindow_Click);
            // 
            // comboBoxQQWindow
            // 
            this.comboBoxQQWindow.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxQQWindow.FormattingEnabled = true;
            this.comboBoxQQWindow.Location = new System.Drawing.Point(44, 49);
            this.comboBoxQQWindow.Name = "comboBoxQQWindow";
            this.comboBoxQQWindow.Size = new System.Drawing.Size(200, 20);
            this.comboBoxQQWindow.TabIndex = 1;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(43, 87);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(83, 12);
            this.label4.TabIndex = 0;
            this.label4.Text = "选择发送按键:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(42, 28);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(95, 12);
            this.label3.TabIndex = 0;
            this.label3.Text = "选择QQ聊天窗口:";
            // 
            // tabPage6
            // 
            this.tabPage6.Controls.Add(this.buttonTestPlay);
            this.tabPage6.Controls.Add(this.comboBoxMusicList);
            this.tabPage6.Controls.Add(this.label5);
            this.tabPage6.Location = new System.Drawing.Point(4, 22);
            this.tabPage6.Name = "tabPage6";
            this.tabPage6.Size = new System.Drawing.Size(577, 165);
            this.tabPage6.TabIndex = 4;
            this.tabPage6.Text = "声音提醒";
            this.tabPage6.UseVisualStyleBackColor = true;
            // 
            // buttonTestPlay
            // 
            this.buttonTestPlay.Location = new System.Drawing.Point(283, 61);
            this.buttonTestPlay.Name = "buttonTestPlay";
            this.buttonTestPlay.Size = new System.Drawing.Size(138, 23);
            this.buttonTestPlay.TabIndex = 3;
            this.buttonTestPlay.Text = "试听";
            this.buttonTestPlay.UseVisualStyleBackColor = true;
            this.buttonTestPlay.Click += new System.EventHandler(this.buttonTestPlay_Click);
            // 
            // comboBoxMusicList
            // 
            this.comboBoxMusicList.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxMusicList.FormattingEnabled = true;
            this.comboBoxMusicList.Location = new System.Drawing.Point(55, 63);
            this.comboBoxMusicList.Name = "comboBoxMusicList";
            this.comboBoxMusicList.Size = new System.Drawing.Size(200, 20);
            this.comboBoxMusicList.TabIndex = 2;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(53, 37);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(59, 12);
            this.label5.TabIndex = 0;
            this.label5.Text = "选择声音:";
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ToolStripMenuItemSeriesSelectAll});
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(101, 26);
            // 
            // ToolStripMenuItemSeriesSelectAll
            // 
            this.ToolStripMenuItemSeriesSelectAll.Name = "ToolStripMenuItemSeriesSelectAll";
            this.ToolStripMenuItemSeriesSelectAll.Size = new System.Drawing.Size(100, 22);
            this.ToolStripMenuItemSeriesSelectAll.Text = "全选";
            // 
            // contextMenuStrip2
            // 
            this.contextMenuStrip2.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ToolStripMenuItemStateSelectAll});
            this.contextMenuStrip2.Name = "contextMenuStrip1";
            this.contextMenuStrip2.Size = new System.Drawing.Size(101, 26);
            // 
            // ToolStripMenuItemStateSelectAll
            // 
            this.ToolStripMenuItemStateSelectAll.Name = "ToolStripMenuItemStateSelectAll";
            this.ToolStripMenuItemStateSelectAll.Size = new System.Drawing.Size(100, 22);
            this.ToolStripMenuItemStateSelectAll.Text = "全选";
            // 
            // toolStrip1
            // 
            this.toolStrip1.ImageScalingSize = new System.Drawing.Size(32, 32);
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripButtonLoginOrLogout,
            this.toolStripButtonQuery,
            this.toolStripButtonMonitor});
            this.toolStrip1.Location = new System.Drawing.Point(0, 0);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Size = new System.Drawing.Size(1043, 39);
            this.toolStrip1.TabIndex = 3;
            this.toolStrip1.Text = "toolStrip1";
            // 
            // toolStripButtonLoginOrLogout
            // 
            this.toolStripButtonLoginOrLogout.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButtonLoginOrLogout.Image")));
            this.toolStripButtonLoginOrLogout.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonLoginOrLogout.Name = "toolStripButtonLoginOrLogout";
            this.toolStripButtonLoginOrLogout.Size = new System.Drawing.Size(97, 36);
            this.toolStripButtonLoginOrLogout.Text = "登陆/注销";
            this.toolStripButtonLoginOrLogout.Click += new System.EventHandler(this.toolStripButtonLoginOrLogout_Click);
            // 
            // toolStripButtonQuery
            // 
            this.toolStripButtonQuery.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButtonQuery.Image")));
            this.toolStripButtonQuery.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonQuery.Name = "toolStripButtonQuery";
            this.toolStripButtonQuery.Size = new System.Drawing.Size(68, 36);
            this.toolStripButtonQuery.Text = "查询";
            this.toolStripButtonQuery.Click += new System.EventHandler(this.toolStripButtonQuery_Click);
            // 
            // toolStripButtonMonitor
            // 
            this.toolStripButtonMonitor.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButtonMonitor.Image")));
            this.toolStripButtonMonitor.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonMonitor.Name = "toolStripButtonMonitor";
            this.toolStripButtonMonitor.Size = new System.Drawing.Size(92, 36);
            this.toolStripButtonMonitor.Text = "开始抢购";
            this.toolStripButtonMonitor.Click += new System.EventHandler(this.toolStripButtonMonitor_Click);
            // 
            // statusStrip1
            // 
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripStatusLabel1});
            this.statusStrip1.Location = new System.Drawing.Point(0, 583);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(1043, 22);
            this.statusStrip1.TabIndex = 4;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // toolStripStatusLabel1
            // 
            this.toolStripStatusLabel1.Name = "toolStripStatusLabel1";
            this.toolStripStatusLabel1.Size = new System.Drawing.Size(32, 17);
            this.toolStripStatusLabel1.Text = "用户";
            // 
            // visualStyler1
            // 
            this.visualStyler1.HostForm = null;
            // 
            // notifyIcon1
            // 
            this.notifyIcon1.Text = "notifyIcon1";
            this.notifyIcon1.Visible = true;
            // 
            // FrmMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1043, 605);
            this.Controls.Add(this.tabControl1);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.toolStrip1);
            this.Controls.Add(this.statusStrip1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "FrmMain";
            this.Text = "斐讯商城抢购助手";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FrmMain_FormClosing);
            this.Load += new System.EventHandler(this.FrmMain_Load);
            this.SizeChanged += new System.EventHandler(this.FrmMain_SizeChanged);
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.panelSeries.ResumeLayout(false);
            this.panelSeries.PerformLayout();
            this.tabPage3.ResumeLayout(false);
            this.tabPage3.PerformLayout();
            this.toolStrip2.ResumeLayout(false);
            this.toolStrip2.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.tabControl2.ResumeLayout(false);
            this.tabPage4.ResumeLayout(false);
            this.tabPage4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownQueryInternal)).EndInit();
            this.tabPage2.ResumeLayout(false);
            this.tabPage2.PerformLayout();
            this.tabPage5.ResumeLayout(false);
            this.tabPage5.PerformLayout();
            this.tabPage6.ResumeLayout(false);
            this.tabPage6.PerformLayout();
            this.contextMenuStrip1.ResumeLayout(false);
            this.contextMenuStrip2.ResumeLayout(false);
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.visualStyler1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel1;
        private System.Windows.Forms.ToolStripButton toolStripButtonQuery;
        private System.Windows.Forms.ToolStripButton toolStripButtonMonitor;
        private SkinSoft.VisualStyler.VisualStyler visualStyler1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Panel panelSeries;
        private System.Windows.Forms.CheckBox checkBoxSeriesAll;
        private System.Windows.Forms.ColumnHeader columnHeader1;
        private System.Windows.Forms.ColumnHeader columnHeader11;
        private System.Windows.Forms.ColumnHeader columnHeader2;
        private System.Windows.Forms.ColumnHeader columnHeader3;
        private System.Windows.Forms.ColumnHeader columnHeader4;
        private System.Windows.Forms.ColumnHeader columnHeader5;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TextBox textBoxOutput;
        private System.Windows.Forms.TabControl tabControl2;
        private System.Windows.Forms.TabPage tabPage4;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip2;
        private System.Windows.Forms.ToolStripMenuItem ToolStripMenuItemStateSelectAll;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
        private System.Windows.Forms.ToolStripMenuItem ToolStripMenuItemSeriesSelectAll;
        private System.Windows.Forms.NotifyIcon notifyIcon1;
        private System.Windows.Forms.TabPage tabPage2;
        private FeiXunHelper.UIs.Common.ColorBlock colorBlock2;
        private System.Windows.Forms.Label label26;
        private TDLib.UIs.Controls.ListViewExNoBlink listViewProductList;
        private System.Windows.Forms.ColumnHeader columnHeader6;
        private System.Windows.Forms.ColumnHeader columnHeader7;
        private System.Windows.Forms.ColumnHeader columnHeader8;
        private System.Windows.Forms.ColumnHeader columnHeader9;
        private System.Windows.Forms.ListBox listBoxCurProductList;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button buttonSelectAddress;
        private System.Windows.Forms.TextBox textBoxCurAddress;
        private System.Windows.Forms.TabPage tabPage3;
        private TDLib.UIs.Controls.ListViewExNoBlink listViewAddressList;
        private System.Windows.Forms.ColumnHeader columnHeader10;
        private System.Windows.Forms.ColumnHeader columnHeader12;
        private System.Windows.Forms.ColumnHeader columnHeader13;
        private System.Windows.Forms.ColumnHeader columnHeader14;
        private System.Windows.Forms.ColumnHeader columnHeader15;
        private System.Windows.Forms.ColumnHeader columnHeader16;
        private System.Windows.Forms.ColumnHeader columnHeader17;
        private System.Windows.Forms.ToolStrip toolStrip2;
        private System.Windows.Forms.ToolStripButton toolStripButtonQueryAddress;
        private System.Windows.Forms.ToolStripButton toolStripButtonAddAddress;
        private System.Windows.Forms.ColumnHeader columnHeader18;
        private System.Windows.Forms.ToolStripButton toolStripButtonLoginOrLogout;
        private System.Windows.Forms.NumericUpDown numericUpDownQueryInternal;
        private System.Windows.Forms.CheckBox checkBoxCustomQueryInternal;
        private System.Windows.Forms.TabPage tabPage5;
        private System.Windows.Forms.TabPage tabPage6;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ComboBox comboBoxQQWindow;
        private System.Windows.Forms.Button buttonGetQQWindow;
        private System.Windows.Forms.Button buttonTestSendQQMsg;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.RadioButton radioButtonCtrlEnter;
        private System.Windows.Forms.RadioButton radioButtonEnter;
        private System.Windows.Forms.ComboBox comboBoxMusicList;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Button buttonTestPlay;
        private System.Windows.Forms.CheckBox checkBoxAutoCreateOrder;

    }
}

