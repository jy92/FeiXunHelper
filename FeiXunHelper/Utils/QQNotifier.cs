﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.InteropServices;
using FeiXunHelper.Definitions;
using System.Windows.Forms;
using System.Threading;

namespace FeiXunHelper.Utils
{
    class QQNotifier
    {
        [DllImport("user32.dll")]
        static extern bool EnumWindows(EnumWindowsProc lpEnumFunc, int lParam);
        public delegate bool EnumWindowsProc(int hwnd, int lParam);
        [DllImport("user32")]
        public static extern int GetClassNameA(int hwnd, StringBuilder lptrString, int nMaxCount);
        [DllImport("user32")]
        public static extern int GetWindowText(int hwnd, StringBuilder lptrString, int nMaxCount);
        [DllImport("user32.dll")]
        static extern bool ShowWindow(IntPtr hWnd, int nCmdShow);
        [DllImport("user32.dll")]
        public static extern bool SetForegroundWindow(IntPtr hWnd);

        /// <summary>
        /// 激活并还原显示窗口
        /// </summary>
        private const int SW_RESTORE = 9;

        private Action<WindowInfo> _addAction = null;

        /// <summary>
        /// 枚举所有窗口相关委托
        /// </summary>
        /// <param name="hwnd">窗口ID</param>
        /// <param name="lParam"></param>
        /// <returns></returns>
        private bool LpEnumFunc(int hwnd, int lParam)
        {
            List<WindowInfo> list = new List<WindowInfo>();

            StringBuilder className = new StringBuilder(254);
            //获取窗口类名
            GetClassNameA(hwnd, className, className.Capacity);
            if (className.ToString().Contains("TXGuiFoundation"))
            {
                //如果为QQ窗口
                StringBuilder title = new StringBuilder(254);
                //获取窗口名称
                GetWindowText(hwnd, title, title.Capacity);
                if (title.ToString().Trim().Length < 1)
                    return true;

                //将获取的窗口名称添加到下拉框中
                if (_addAction != null)
                {
                    _addAction(new WindowInfo() { Hwnd = (IntPtr)hwnd, Name = title.ToString()});
                }
            }
            return true;
        }

        public void FillQQWindowList(Action<WindowInfo> addAction)
        {
            _addAction = addAction;
            EnumWindows(LpEnumFunc, 0);
        }

        /// <summary>
        /// 发送QQ消息
        /// </summary>
        /// <param name="message"></param>
        public void SendQQMessage(IntPtr hwnd, string message, QQSendKeyMode mode)
        {
            ShowWindow(hwnd, SW_RESTORE);
            SetForegroundWindow(hwnd);
            SendKeys.SendWait(message);
            if (mode == QQSendKeyMode.Enter)
            {
                SendKeys.SendWait("{Enter}");
            }
            else
            {
                /*
                 *  SHIFT    +     
                    CTRL    ^     
                    ALT    %
                 */
                SendKeys.SendWait("^{Enter}");
            }
        }
    }

    /// <summary>
    /// 消息发送模式
    /// </summary>
    public enum QQSendKeyMode
    {
        Enter,
        CtrlEnter
    }
}
